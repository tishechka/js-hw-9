/*
1) Опишіть, як можна створити новий HTML тег на сторінці.

Потрібно використати метод document.createElement('tagName'), де 'tagName' - це ім'я нового тегу.

2) Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

insertAdjacentHTML дозволяє вставляти HTML-код у вказаному місці елемента.
Перший параметр insertAdjacentHTML визначає позицію, де буде вставлено HTML-код відносно елемента.
    'beforebegin': Вставити HTML-код прямо перед елементом.
    'afterbegin': Вставити HTML-код всередині елемента, перед його першим дочірнім елементом.
    'beforeend': Вставити HTML-код всередині елемента, після його останнього дочірнього елемента.
    'afterend': Вставити HTML-код прямо після елемента.

3) Як можна видалити елемент зі сторінки?

Метод remove() або removeChild() в залежності від того, як саме треба видалити елемент.
*/

function arrList(arr, parent) {

    const ol = document.createElement("ol");

    for (let i = 0; i < array.length; i++) {
        const li = document.createElement("li");
        li.textContent = arr[i];
        ol.appendChild(li);
    }

    parent.appendChild(ol);
}

array = ["1", "2", "3", "sea", "user", 23];

arrList(array, document.body);